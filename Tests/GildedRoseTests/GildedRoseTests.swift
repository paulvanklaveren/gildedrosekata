@testable import GildedRose
import XCTest

class GildedRoseTests: XCTestCase {

    func testThatUpdatingQualityWillNotAffectName() {
        let items = [Item(name: "Rose", sellIn: 0, quality: 0)]
        let app = GildedRose(items: items)
        app.updateQuality()
        XCTAssertEqual("Rose", app.items[0].name)
    }
    
    func testThatForNormalItemsSellInAndQualityDecreaseEachDay() {
        let app = GildedRose(items: [ItemFactory.makeNormal(sellIn: 10, quality: 10)])
        app.updateQuality()
        let updatedItem = ItemFactory.makeNormal(sellIn: 9, quality: 9)
        XCTAssertEqual(app.items[0], updatedItem)
    }
    
    func testThatForNormalItemsQualityDecreasesTwiceAsFastAfterSellByDate() {
        let quality = 10
        let normalItem = ItemFactory.makeNormal(sellIn: 10, quality: quality)
        let expiredNormalItem = ItemFactory.makeNormal(sellIn: 0, quality: quality)
        
        let app = GildedRose(items: [normalItem, expiredNormalItem])
        app.updateQuality()
        
        let decreaseNormalItem = quality - app.items[0].quality
        let decreaseExpiredNormalItem = quality - app.items[1].quality
        XCTAssertEqual(decreaseExpiredNormalItem / decreaseNormalItem, 2)
    }
    
    func testThatForAllItemsTheQualityIsNeverNegative() { // TODO: Check the definitions of "all" & "never".
        let normalItem = ItemFactory.makeNormal(sellIn: 0, quality: 0)
        let agedBrie = ItemFactory.makeAgedBrie(sellIn: 0, quality: 0)
        let backstagePasses = ItemFactory.makeBackstagePasses(sellIn: 0, quality: 0)
        let sulfuras = ItemFactory.makeSulfuras(sellIn: 0, quality: 80)
        
        let app = GildedRose(items: [normalItem, agedBrie, backstagePasses, sulfuras])
        app.updateQuality()
        
        app.items.forEach { XCTAssert($0.quality >= 0) }
    }
    
    func testThatForAgedBrieQualityAlwaysIncreases() {
        let quality = 10
        let app = GildedRose(items: [ItemFactory.makeAgedBrie(sellIn: 10, quality: quality),
                                     ItemFactory.makeAgedBrie(sellIn: 0, quality: quality),
                                     ItemFactory.makeAgedBrie(sellIn: -10, quality: quality)])
        app.updateQuality()
        
        app.items.forEach { XCTAssert($0.quality > quality) }
    }
    
    func testThatForAgedBrieQualityIncreasesTwiceAsFastAfterSellByDate() {
        let quality = 10
        let agedBrie = ItemFactory.makeAgedBrie(sellIn: 10, quality: quality)
        let expiredAgedBrie = ItemFactory.makeAgedBrie(sellIn: 0, quality: quality)
        
        let app = GildedRose(items: [agedBrie, expiredAgedBrie])
        app.updateQuality()
        
        let decreaseAgedBrie = quality - app.items[0].quality
        let decreaseExpiredAgedBrie = quality - app.items[1].quality
        XCTAssertEqual(decreaseExpiredAgedBrie / decreaseAgedBrie, 2)
    }
    
    func testThatForAllNonLegendaryItemsTheQualityCannotExceed50() {
        let itemsThatIncreaseInQuality = [ItemFactory.makeAgedBrie(sellIn: 10, quality: 50),
                                          ItemFactory.makeBackstagePasses(sellIn: 10, quality: 50)]
        let app = GildedRose(items: itemsThatIncreaseInQuality)
        app.updateQuality()
        
        app.items.forEach { XCTAssert($0.quality == 50) }
    }
    
    func testThatForSulfurasQualityIsAlways80() {
        // NOTE: `sellIn` values are based on the example values from main.swift.
        let app = GildedRose(items: [ItemFactory.makeSulfuras(sellIn: 1, quality: 80),
                                     ItemFactory.makeSulfuras(sellIn: 0, quality: 80),
                                     ItemFactory.makeSulfuras(sellIn: -1, quality: 80)])
        app.updateQuality()
        
        app.items.forEach { XCTAssert($0.quality == 80) }
    }
    
    func testThatForBackstagePassesQualityIncreasesByOneWhenSellInExceeds10Days() {
        let app = GildedRose(items: [ItemFactory.makeBackstagePasses(sellIn: 20, quality: 0)])
        app.updateQuality()
        XCTAssertEqual(app.items[0].quality, 1)
    }
    
    func testThatForBackstagePassesQualityIncreasesByTwoWhenSellInIs10To5Days() {
        let items = [Int](6...10).map { ItemFactory.makeBackstagePasses(sellIn: $0, quality: 0) }
        let app = GildedRose(items: items)
        app.updateQuality()
        app.items.forEach { XCTAssert($0.quality == 2) }
    }
    
    func testThatForBackstagePassesQualityIncreasesByThreeWhenSellInIs5OrLessDays() {
        let items = [Int](1...5).map { ItemFactory.makeBackstagePasses(sellIn: $0, quality: 0) }
        let app = GildedRose(items: items)
        app.updateQuality()
        app.items.forEach { XCTAssert($0.quality == 3) }
    }
    
    func testThatForBackstagePassesQualityDropToZeroAfterSellByDate() {
        let app = GildedRose(items: [ItemFactory.makeBackstagePasses(sellIn: 0, quality: 50)])
        app.updateQuality()
        XCTAssertEqual(app.items[0].quality, 0)
    }

    func testThatForConjuredItemsQualityDecreasesTwiceAsFastAsNormalItems() {
        let quality = 10
        let normalItem = ItemFactory.makeNormal(sellIn: 10, quality: quality)
        let expiredNormalItem = ItemFactory.makeNormal(sellIn: 0, quality: quality)
        let conjuredItem = ItemFactory.makeConjuredItem(sellIn: 10, quality: quality)
        let expiredConjuredItem = ItemFactory.makeConjuredItem(sellIn: 0, quality: quality)

        let app = GildedRose(items: [normalItem, expiredNormalItem, conjuredItem, expiredConjuredItem])
        app.updateQuality()
        
        let decreaseNormalItem = quality - app.items[0].quality
        let decreaseExpiredNormalItem = quality - app.items[1].quality
        let decreaseConjuredItem = quality - app.items[2].quality
        let decreaseExpiredConjuredItem = quality - app.items[3].quality
        XCTAssertEqual(decreaseConjuredItem / decreaseNormalItem, 2)
        XCTAssertEqual(decreaseExpiredConjuredItem / decreaseExpiredNormalItem, 2)
    }
    
    static var allTests : [(String, (GildedRoseTests) -> () throws -> Void)] {
        return [
            ("testThatUpdatingQualityWillNotAffectName", testThatUpdatingQualityWillNotAffectName),
            ("testThatForNormalItemsSellInAndQualityDecreaseEachDay", testThatForNormalItemsSellInAndQualityDecreaseEachDay),
            ("testThatForNormalItemsQualityDecreasesTwiceAsFastAfterSellByDate", testThatForNormalItemsQualityDecreasesTwiceAsFastAfterSellByDate),
            ("testThatForAllItemsTheQualityIsNeverNegative", testThatForAllItemsTheQualityIsNeverNegative),
            ("testThatForAgedBrieQualityAlwaysIncreases", testThatForAgedBrieQualityAlwaysIncreases),
            ("testThatForAllNonLegendaryItemsTheQualityCannotExceed50", testThatForAllNonLegendaryItemsTheQualityCannotExceed50),
            ("testThatForSulfurasQualityIsAlways80", testThatForSulfurasQualityIsAlways80),
            ("testThatForBackstagePassesQualityIncreasesByOneWhenSellInExceeds10Days", testThatForBackstagePassesQualityIncreasesByOneWhenSellInExceeds10Days),
            ("testThatForBackstagePassesQualityIncreasesByTwoWhenSellInIs10To5Days", testThatForBackstagePassesQualityIncreasesByTwoWhenSellInIs10To5Days),
            ("testThatForBackstagePassesQualityIncreasesByThreeWhenSellInIs5OrLessDays", testThatForBackstagePassesQualityIncreasesByThreeWhenSellInIs5OrLessDays),
            ("testThatForBackstagePassesQualityDropToZeroAfterSellByDate", testThatForBackstagePassesQualityDropToZeroAfterSellByDate),
            ("testThatForConjuredItemsQualityDecreasesTwiceAsFastAsNormalItems", testThatForConjuredItemsQualityDecreasesTwiceAsFastAsNormalItems)
        ]
    }
}


struct ItemFactory {
    static func makeNormal(sellIn: Int, quality: Int) -> NormalItem {
        NormalItem(name: "normal", sellIn: sellIn, quality: quality)
    }
    static func makeAgedBrie(sellIn: Int, quality: Int) -> AgedBrie {
        AgedBrie(name: "Aged Brie", sellIn: sellIn, quality: quality)
    }
    static func makeBackstagePasses(sellIn: Int, quality: Int) -> BackstagePasses {
        BackstagePasses(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: sellIn, quality: quality)
    }
    static func makeSulfuras(sellIn: Int, quality: Int) -> Item {
        Item(name: "Sulfuras, Hand of Ragnaros", sellIn: sellIn, quality: quality)
    }
    static func makeConjuredItem(sellIn: Int, quality: Int) -> ConjuredItem {
        ConjuredItem(name: "Conjured Mana Cake", sellIn: sellIn, quality: quality)
    }
}
