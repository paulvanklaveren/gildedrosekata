public class GildedRose {
    var items: [Item]
    
    public init(items: [Item]) {
        self.items = items.map {
            switch $0.name {
                case "Sulfuras, Hand of Ragnaros": return $0
                case "Aged Brie": return AgedBrie(item: $0)
                case "Backstage passes to a TAFKAL80ETC concert": return BackstagePasses(item: $0)
                case "Conjured Mana Cake": return ConjuredItem(item: $0)
                default: return NormalItem(item: $0)
            }
        }
    }
    
    @discardableResult
    public func updateQuality() -> [Item] {
        items.forEach {
            ($0 as? Updatable)?.update()
        }
        return items
    }
}
