public class Item {
    public var name: String
    public var sellIn: Int
    public var quality: Int
    
    public init(name: String, sellIn: Int, quality: Int) {
        self.name = name
        self.sellIn = sellIn
        self.quality = quality
    }
}

extension Item: CustomStringConvertible {
    public var description: String {
        return self.name + ", " + String(self.sellIn) + ", " + String(self.quality)
    }
}

extension Item: Equatable {
    public static func == (lhs: Item, rhs: Item) -> Bool {
        lhs.name == rhs.name &&
            lhs.sellIn == rhs.sellIn &&
            lhs.quality == rhs.quality
    }
}

extension Item {
    convenience init(item: Item) {
        self.init(name: item.name, sellIn: item.sellIn, quality: item.quality)
    }
}

// MARK: -

protocol Updatable {
    func update()
}

class AgedBrie: Item, Updatable {
    func update() {
        quality = min(50, sellIn > 0 ? quality + 1 : quality + 2)
        sellIn -= 1
    }
}

class BackstagePasses: Item, Updatable {
    func update() {
        switch sellIn {
            case ...0: quality = 0
            case 1...5: quality += 3
            case 6...10: quality += 2
            default: quality += 1
        }
        quality = min(50, quality)
        sellIn -= 1
    }
}

class NormalItem: Item, Updatable {
    func update() {
        quality = max(0, sellIn > 0 ? quality - 1 : quality - 2)
        sellIn -= 1
    }
}

class ConjuredItem: Item, Updatable {
    func update() {
        quality = max(0, sellIn > 0 ? quality - 2 : quality - 4)
        sellIn -= 1
    }
}
